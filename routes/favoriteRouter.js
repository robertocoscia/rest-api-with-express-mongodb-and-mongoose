var express = require("express");
var bodyParser = require("body-parser");
var Users = require("../models/user");
var Favorites = require("../models/favorite");
var passport = require("passport");
var authenticate = require("../authenticate");
const cors = require("./cors");
var router = express.Router();
router.use(bodyParser.json());
const favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());
/* GET users listing. */
favoriteRouter
  .route("/")
  .get(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({ user: req.user })
      .populate("user")
      .populate("dishes")
      .then(
        favorite => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          if (favorite.length) {
            res.json(favorite[0]);
          } else {
            res.json(favorite);
          }
        },
        err => {
          next(err);
        }
      )
      .catch(err => next(err));
  })

  .post(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({ user: req.user })
      .then(favorite => {
        if (favorite.length) {
          var id = favorite[0]._id;
          var update_dishes = favorite[0].dishes;
          // console.log(update_dishes);
          // console.log(req.body.length);
          for (var i = 0; i < req.body.length; i++) {
            // console.log(req.body[i]._id);
            if (favorite[0].dishes.indexOf(req.body[i]._id) === -1) {
              update_dishes.push(req.body[i]._id);
            }
          }

          // res.statusCode = 200;
          // res.setHeader("Content-Type", "application/json");
          // res.json(update_dishes);
          // return;

          Favorites.findByIdAndUpdate(
            id,
            { $set: { dishes: update_dishes } },
            { new: true }
          )
            .then(
              favorite => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(favorite);
              },
              err => next(err)
            )
            .catch(err => next(err));
        } else {
          var new_favorites = [];
          for (var i = 0; i < req.body.length; i++) {
            if (new_favorites.indexOf(req.body[i]._id) === -1) {
              new_favorites.push(req.body[i]._id);
            }
          }

          Favorites.create(
            { user: req.user, dishes: new_favorites },
            (err, favorite) => {
              if (err) {
                return next(err);
              } else {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(favorite);
              }
            }
          );
        }
      })
      .catch(err => next(err));
  })

  .delete(authenticate.verifyUser, (req, res, next) => {
    Favorites.findOneAndRemove({ user: req.user }, (err, favorite) => {
      if (err) {
        return next(err);
      } else {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(favorite);
      }
    });
  });

favoriteRouter
  .route("/:dishId")
  .post(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({ user: req.user })
      .then(
        favorite => {
          if (favorite.length) {
            var id = favorite[0]._id;
            Favorites.findById(id)
              .then(
                favorite => {
                  if (favorite.dishes.indexOf(req.params.dishId) === -1) {
                    favorite.dishes.push(req.params.dishId);
                    favorite.save((err, favorite) => {
                      if (err) {
                        return next(err);
                      } else {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(favorite);
                      }
                    });
                  }
                },
                err => next(err)
              )
              .catch(err => next(err));
          } else {
            Favorites.create({ user: req.user }, (err, favorite) => {
              if (err) {
                return next(err);
              }

              if (favorite.dishes.indexOf(req.params.dishId) === -1) {
                favorite.dishes.push(req.params.dishId);

                favorite.save((err, favorite) => {
                  if (err != null) {
                    return next(err);
                  } else {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(favorite);
                  }
                });
              }
            });
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })

  .delete(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({ user: req.user })
      .then(
        favorite => {
          if (!favorite.length) {
            err = new Error(
              "You cannot find the record associated with that user!"
            );
            err.status = 404;
            return next(err);
          } else {
            var id = favorite[0]._id;
            Favorites.findById(id)
              .then(
                favorite => {
                  var index = favorite.dishes.indexOf(req.params.dishId);
                  if (index === -1) {
                    err = new Error(
                      "The dish you want to delete does not exist!"
                    );
                    err.status = 404;
                    return next(err);
                  } else {
                    favorite.dishes.splice(index, 1);
                    favorite.save((err, favorite) => {
                      if (err != null) {
                        return next(err);
                      } else {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(favorite);
                      }
                    });
                  }
                },
                err => next(err)
              )
              .catch(err => next(err));
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  });

module.exports = favoriteRouter;
